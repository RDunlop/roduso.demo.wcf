﻿using System;
using Roduso.Demo.Commands.ArticleHandling;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;

namespace Roduso.Demo.TestWcfConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ArticleServiceClient articleClient = new ArticleServiceClient();
            var response = articleClient.AddArticle(new AddArticleCommand {Name = "A1", Amount = 1.2});

            OrderServiceClient client = new OrderServiceClient();
            client.PlaceOrder(new PlaceOrderCommand { ArticleId = response.NewArticleId, NumberOfArticles = 2 });

            Console.ReadKey();
        }
    }
}
