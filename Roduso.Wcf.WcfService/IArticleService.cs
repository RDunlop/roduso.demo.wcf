﻿using System.ServiceModel;
using Roduso.Demo.Commands.ArticleHandling;

namespace Roduso.Wcf.WcfService
{
    [ServiceContract]
    public interface IArticleService
    {
        [OperationContract]
        AddArticleResponse AddArticle(AddArticleCommand addArticleCommand);
    }
}