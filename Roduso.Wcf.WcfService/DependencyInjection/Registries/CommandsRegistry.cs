﻿using Roduso.Demo.Commands.Communication;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using StructureMap;

namespace Roduso.Wcf.WcfService.DependencyInjection.Registries
{
    public class CommandsRegistry : Registry
    {
        public CommandsRegistry()
        {
            Scan(scan =>
            {
                scan.AssemblyContainingType<IPlaceOrderCommandHandler>();
                scan.WithDefaultConventions();
            });

            For<ICommunicator>().Use<CommunicationInterceptor>();
        }
    }
}