﻿using Roduso.Demo.Model;
using StructureMap;

namespace Roduso.Wcf.WcfService.DependencyInjection.Registries
{
    public class ModelRegistry : Registry
    {
        public ModelRegistry()
        {
            Scan(scan =>
            {
                scan.AssemblyContainingType<IContextFactory>();
                scan.WithDefaultConventions();
            });

            For<IContextFactory>().Singleton().Use<ContextFactory>();
        }
    }
}