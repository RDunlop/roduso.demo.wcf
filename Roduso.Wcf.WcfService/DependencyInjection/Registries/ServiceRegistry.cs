﻿using StructureMap;

namespace Roduso.Wcf.WcfService.DependencyInjection.Registries
{
    public class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });
        }
    }
}