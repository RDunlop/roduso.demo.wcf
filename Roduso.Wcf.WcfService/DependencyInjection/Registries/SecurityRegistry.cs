﻿using Roduso.Demo.Security;
using StructureMap;

namespace Roduso.Wcf.WcfService.DependencyInjection.Registries
{
    public class SecurityRegistry : Registry
    {
        public SecurityRegistry()
        {
            Scan(scan =>
            {
                scan.AssemblyContainingType<IPrincipal>();
                scan.WithDefaultConventions();
            });
        }   
    }
}