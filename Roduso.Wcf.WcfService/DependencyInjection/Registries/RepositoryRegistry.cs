﻿using Roduso.Demo.Repositories;
using StructureMap;

namespace Roduso.Wcf.WcfService.DependencyInjection.Registries
{
    public class RepositoryRegistry : Registry
    {
        public RepositoryRegistry()
        {
            Scan(scan =>
            {
                scan.AssemblyContainingType<IOrderRepository>();
                scan.WithDefaultConventions();
            });
        }
    }
}