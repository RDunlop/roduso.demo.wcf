﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Roduso.Wcf.WcfService.DependencyInjection.Registries;

namespace Roduso.Wcf.WcfService.DependencyInjection
{
    public class StructureMapServiceHostFactory : ServiceHostFactory
    {
        public StructureMapServiceHostFactory()
        {            
            ObjectFactory.Initialize<ServiceRegistry>();
            ObjectFactory.Initialize<ModelRegistry>();
            ObjectFactory.Initialize<CommandsRegistry>();
            ObjectFactory.Initialize<RepositoryRegistry>();
            ObjectFactory.Initialize<SecurityRegistry>();
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new StructureMapServiceHost(serviceType, baseAddresses);
        }
    }
}