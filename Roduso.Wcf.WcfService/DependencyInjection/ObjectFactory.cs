﻿using System;
using System.Threading;
using StructureMap;

namespace Roduso.Wcf.WcfService.DependencyInjection
{
    public static class ObjectFactory
    {
        private static readonly Lazy<Container> ContainerBuilder = new Lazy<Container>(() => new Container(), LazyThreadSafetyMode.ExecutionAndPublication);

        public static IContainer Container => ContainerBuilder.Value;

        public static void Initialize<T>() where T : Registry, new()
        {
            Container.Configure(x => { x.AddRegistry<T>(); });
        }
    }
}