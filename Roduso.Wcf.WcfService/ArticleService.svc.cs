﻿using Roduso.Demo.Commands.ArticleHandling;

namespace Roduso.Wcf.WcfService
{
    public class ArticleService : IArticleService
    {
        private readonly IAddArticleCommandHandler _addArticleCommandHandler;

        public ArticleService(IAddArticleCommandHandler addArticleCommandHandler)
        {
            _addArticleCommandHandler = addArticleCommandHandler;
        }

        public AddArticleResponse AddArticle(AddArticleCommand addArticleCommand)
        {
            return _addArticleCommandHandler.AddArticle(addArticleCommand);
        }
    }
}