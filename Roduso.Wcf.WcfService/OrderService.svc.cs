﻿using Roduso.Demo.Commands.Orderhandling.PlaceOrder;

namespace Roduso.Wcf.WcfService
{
    public class OrderService : IOrderService
    {
        private readonly IPlaceOrderCommandHandler _placeOrderCommandHandler;

        public OrderService(IPlaceOrderCommandHandler placeOrderCommandHandler)
        {
            _placeOrderCommandHandler = placeOrderCommandHandler;
        }

        public void PlaceOrder(PlaceOrderCommand placeOrderCommand)
        {
            _placeOrderCommandHandler.PlaceOrder(placeOrderCommand);
        }
    }
}
