﻿using System.ServiceModel;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;

namespace Roduso.Wcf.WcfService
{
    [ServiceContract]
    public interface IOrderService
    {
        [OperationContract]
        void PlaceOrder(PlaceOrderCommand placeOrderCommand);
    }
}
