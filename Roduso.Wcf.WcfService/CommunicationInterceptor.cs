using System;
using Roduso.Demo.Commands.Communication;

namespace Roduso.Wcf.WcfService
{
    public class CommunicationInterceptor : ICommunicator
    {
        public void NotifyCustomer(string message)
        {
            Console.WriteLine(message);            
        }
    }
}