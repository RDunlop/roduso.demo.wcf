# README #


### What is this repository for? ###

* WCF using StructureMap demo
* 0.1

### How do I get set up? ###

* This demo is based on a model found in https://bitbucket.org/RDunlop/roduso.demo.command.query.separation
* To download the model a package source need to be added in VIsual Studio (Tools -> NuGet Package Manager -> Package Manager Settings -> Package Sources): https://www.myget.org/F/roduso-demo/api/v2
* It is based on an article by Jimmy Bogard about Integrating StructureMap with WCF:
https://lostechies.com/jimmybogard/2008/07/30/integrating-structuremap-with-wcf/

* The meat of the demo is found in the folder/name space DependencyInjection. The rest of the code (model etc. + console app) is just to make sure it actually works.
